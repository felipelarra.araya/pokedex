
const pokemonContainer = document.querySelector(".pokemon-container");
const previous = document.querySelector("#previous");
const next = document.querySelector("#next");
const btn_buscar = document.querySelector("#btn_buscar");
const pokemon_name = document.querySelector("#pokemon_name");
const poke_abi = document.getElementById('poke_abi');
const poke_name = document.getElementById('poke_name');
const label_experiencia = document.getElementById('label_experiencia');
const label_height = document.getElementById('label_height');
const labe_weight = document.getElementById('label_weight');
const card_details = document.getElementById('card_details');


let limit = 9;
let offset = 1;

previous.addEventListener("click", () => {
  if (offset != 1) {
    offset -= 9;
    removeChildNodes(pokemonContainer);
    fetchPokemons(offset, limit);
  }
});

next.addEventListener("click", () => {
  offset += 9;
  removeChildNodes(pokemonContainer);
  fetchPokemons(offset, limit);
});

btn_buscar.addEventListener("click", () => {
  
    removeChildNodes(pokemonContainer);
    fetchPokemon(pokemon_name.value.toLowerCase());

    if(pokemon_name.value.length == 0){
        fetchPokemons(offset, limit);
    }

    
});

function fetchPokemon(id) {
  fetch(`https://pokeapi.co/api/v2/pokemon/${id}/`)
    .then((res) => res.json())
    .then((data) => {
      createPokemon(data);
      
      
    }).catch((error) => {
        card_details.style.display="none";
        alert('Pokemon no encontrado');
});
}

function fetchPokemonDetail(id) {
  fetch(`https://pokeapi.co/api/v2/pokemon/${id}/`)
    .then((res) => res.json())
    .then((data) => {
      console.log(data);
      
    });
}

function fetchPokemons(offset, limit) {
  
  for (let i = offset; i <= offset + limit; i++) {
    fetchPokemon(i);
  }
}

function createPokemon(pokemon) {
 
  const card = document.createElement("div");
  card.classList.add("pokemon-block");
  const spriteContainer = document.createElement("div");
  spriteContainer.classList.add("img-container");
  const sprite = document.createElement("img");
  sprite.src = pokemon.sprites.front_default;
  spriteContainer.appendChild(sprite);
  const number = document.createElement("p");
  number.textContent = `#${pokemon.id.toString().padStart(3, 0)}`;
  const name = document.createElement("p");
  name.classList.add("name");
  name.textContent = pokemon.name;

  card.appendChild(spriteContainer);
  card.appendChild(number);
  card.appendChild(name);
  pokemonContainer.appendChild(card);

  card.addEventListener( 'click', function() {

    document.getElementById("pokemon-img").src = pokemon.sprites.front_default;
    document.getElementById("poke_name").innerText = pokemon.name;
    document.getElementById("poke_base_experience").innerText = pokemon.base_experience;
    document.getElementById("poke_height").innerText = pokemon.height;
    document.getElementById("poke_weight").innerText = pokemon.weight;
    label_experience.style.display="block";
    label_height.style.display="block";
    label_weight.style.display="block";
    card_details.style.display="block";

    while (poke_abi.firstChild) {
        poke_abi.firstChild.remove();
    }
   
  let abilities = pokemon.abilities;
    abilities.forEach(ability => {
      let ability_span = document.createElement("span");
      ability_span.innerText = ability.ability.name;
      ability_span.classList.add("abilities-box");
      poke_abi.append(ability_span);
   
});
    
    

  });
}



function removeChildNodes(parent) {
  while (parent.firstChild) {
    parent.removeChild(parent.firstChild);
  }
}



fetchPokemons(offset, limit)
