<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Log;

use App\Models\Pokemon;
use App\Models\Ability;

class UpdatePokemon extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'pokemon:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Comando para actualizar pokemones';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

    $pokemons = [];
    $url = 'https://pokeapi.co/api/v2/pokemon/';
    
    try {
        for ($i=1; $i <= 100 ; $i++) { 
    
            $response = Http::get($url.$i.'/');
            $pokemons[] = $response->json();
            }

        //Pendiente  guardar las habilidades en su tabla y en la tabla pivot
        Pokemon::updateOrCreate(
            ['name' => $pokemons['name']],
            [
                'height' => $pokemons['height'],
                'weight' => $pokemons['weight'],
                'base_experience' => $pokemons['base_experience']
            ]
        );
    }
    catch(\Exception $e){
            
    Log::error(__METHOD__ . ' :: ' . $e->getMessage());
    Log::error(__METHOD__ . ' :: ' . $e->getTraceAsString());
    }
}
}