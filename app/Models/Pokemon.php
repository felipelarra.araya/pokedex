<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Ability;

class Pokemon extends Model
{
    use HasFactory;
    protected $table = "pokemons";

    protected $fillable = [
        'name', 'base_experience', 'height', 'weight'
    ];


    public function abilities()
    {
        return $this->belongsToMany(Ability::class, 'pokemons_abilities', 'pokemon_id', 'ability_id');
    }
    
}
