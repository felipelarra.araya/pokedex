<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pokemon;

class Ability extends Model
{
    use HasFactory;
    

    protected $fillable = [
        'name'
    ];

    public function pokemons()
    {
        return $this->belongsToMany(Pokemon::class, 'portfolio_category', 'ability_id', 'pokemon_id');
    }
}
