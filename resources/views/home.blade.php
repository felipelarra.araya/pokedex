
@extends('main')
@section('css')
@stop
@section('contenido')


<img  src="https://raw.githubusercontent.com/PokeAPI/media/master/logo/pokeapi_256.png">


    <div class="container">
        <div class="input-group mb-3">
            <input type="text" id="pokemon_name" class="form-control" placeholder="Escribe el nombre del Pokemon..." >
            <div class="input-group-append">
              <button class="btn btn-outline-primary" type="button" id="btn_buscar">Buscar</button>
            </div>
          </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="pokemon-container"></div>
                <nav class="pagination" aria-label="...">
                    <ul class="pagination">
                        <li class="page-item" id="previous">
                            <a class="page-link" href="#" tabindex="-1">Anterior</a>
                        </li>
                        <li class="page-item" id="next">
                            <a class="page-link" href="#">Siguiente</a>
                        </li>
                    </ul>
                </nav>
            </div>
    
            <div class="col-md-6">
                <div class="card mb-3" id="card_details" style="max-width: 440px; display:none;">
                    <div class="row no-gutters">
                        <div class="col-md-4">
                            <img id="pokemon-img" src="">
                        </div>
                        <div class="col-md-8">
                            <div class="card-body">
                                <div class="card-title" id="poke_name"></div>
                                <p class="card-text">
                                    <div data-poke-abilities id="poke_abi" class="poke-abilitites"></div>
                                    <label id="label_experience" style="display: none">Experiencia:</label>
                                    <div id="poke_base_experience"></div>
                                    <label id="label_height" style="display: none">Altura:</label>
                                    <div id="poke_height"></div>
                                    <label id="label_weight" style="display: none">Peso:</label>
                                    <div id="poke_weight"></div>
                                </p>
    
                            </div>
                        </div>
                    </div>
                </div>
    
            </div>
        </div>
    </div>

    
    
    

@endsection


@section('js')


@endsection
