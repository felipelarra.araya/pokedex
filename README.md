# Prueba MZZO PokeApi




Prueba MZZO .

- Laravel
- Javascript
- Mysql


## Installation

 

Clonar repositorio

```sh
https://gitlab.com/felipelarra.araya/pokedex.git
```

Cambiar a la carpeta del repositorio

```sh
cd pokedex
```
Instala todas las dependencias usando composer
```sh
composer install
```
Copie el archivo env de ejemplo y realice los cambios de configuración necesarios en el archivo .env

Generar una nueva clave de aplicación
```sh
php artisan key:generate
```
Ejecute las migraciones de la base de datos (establezca la conexión de la base de datos en .env antes de migrar)
```sh
php artisan migrate
```
Inicie el servidor de desarrollo local
```sh
php artisan serve
```
Ahora puede acceder al servidor en http://localhost:8000

